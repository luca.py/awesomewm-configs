-- ## Bar ##
-- ~~~~~~~~~

-- Requirements :
-- ~~~~~~~~~~~~~~
local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

-- # Bar Widgets :
-- ~~~~~~~~~~~~~~~
local taglist = require("ui.bar.taglist")
local tasklist = require("ui.bar.tasklist")
local layoutbox = require("ui.bar.layoutbox")
mylayoutbox = wibox.container.margin(layoutbox(s), dpi(0), dpi(0), dpi(6), dpi(6))
mylauncher = wibox.container.margin(mylauncher, dpi(0), dpi(0), dpi(6), dpi(6))


-- # Widgets :
-- ~~~~~~~~~~~
-- # Keyboard : 
mykeyboardlayout = awful.widget.keyboardlayout()
-- # Clock : 
mytextclock = wibox.widget.textclock()


-- Systray
local systray = wibox.widget {
	visible = true,
	base_size = dpi(30),
	horizontal = true,
	screen = 'primary',
	{
		{
			wibox.widget.systray,
			layout = wibox.layout.fixed.horizontal,
		},
		margins = {top = dpi(6), bottom = dpi(6), left = dpi(6), right = dpi(6)},
		widget = wibox.container.margin,
	},
	margins = {top = dpi(2), bottom = dpi(2)},
	widget = wibox.container.margin,
}


local function get_bar(s)
    -- Create the wibox
	s.mywibar = awful.wibar({
		position = "bottom",
		type = "tooltip",
		ontop = false,
		stretch = true,
		visible = true,
		height = dpi(42),
		width = s.geometry.width,
		screen = s,
		bg = colors.black,
		--bg = colors.main_transparent,
		--bg = colors.transparent,
		--opacity = 0.85,
	})
 
	--s.mywibar:struts { bottom = dpi(60), top = dpi(20), left = dpi(20), right = dpi(20) }
 
    -- Bar setup :
    s.mywibar:setup {
		{
			{
				{
					{
						mylauncher,
						taglist(s),
						--tasklist(s),
						spacing = dpi(10),
						layout = wibox.layout.fixed.horizontal
					},
					--nil,
					-- # Tasks in middel :
					{
						tasklist(s),
						layout = wibox.layout.fixed.horizontal
					},
					{
						mykeyboardlayout,
						mytextclock,
						systray,
						mylayoutbox,
						layout = wibox.layout.fixed.horizontal,
						spacing = dpi(10)
					},
					layout = wibox.layout.align.horizontal,
					expand = "none"
				},
				widget = wibox.container.margin,
				margins = {left = dpi(15), right = dpi(15), top = dpi(2), bottom = dpi(2)}
			},
			widget = wibox.container.background,
			bg = colors.bg_color,
			forced_height = s.mywibar.height
		},
		layout = wibox.layout.fixed.vertical,
		spacing = dpi(10)
    }  
    
    
    -- function to remove the bar in maxmized/fullscreen apps
	local function remove_wibar(c)
		if c.fullscreen or c.maximized then
			c.screen.mywibar.visible = false
		else
			c.screen.mywibar.visible = true
		end
    end

    local function add_wibar(c)
        if c.fullscreen or c.maximized then
            c.screen.mywibar.visible = true
        end
    end
    client.connect_signal("property::fullscreen", remove_wibar)
    client.connect_signal("request::unmanage", add_wibar)
 
end

screen.connect_signal("request::desktop_decoration", function(s)
	get_bar(s)
end)

