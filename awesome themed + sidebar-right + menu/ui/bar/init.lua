-- ## Bar ##
-- ~~~~~~~~~

-- Requirements :
-- ~~~~~~~~~~~~~~
local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

-- # Libs :
-- ~~~~~~~~
local helpers = require("libs.helpers")

-- # Main Bar Widgets :
-- ~~~~~~~~~~~~~~~~~~~~
local taglist = require("ui.bar.taglist")
local tasklist = require("ui.bar.tasklist")
local layoutbox = require("ui.bar.layoutbox")
mylayoutbox = wibox.container.margin(layoutbox(s), dpi(0), dpi(0), dpi(6), dpi(6))
mylauncher = wibox.container.margin(mylauncher, dpi(0), dpi(0), dpi(6), dpi(6))

-- # Widgets :
-- ~~~~~~~~~~~
-- # Sidebar button :
local sidebar_button = require('ui.widgets.sidebar_button')
-- # Systray :
local systray = require('ui.widgets.systray')
-- # Clock : 
local clock_widget = require('ui.widgets.clock')
-- # Keyboard : 
local keyboard_widget = require('ui.widgets.keyboardlayout')
-- # Launcher :
local launcher = require('ui.widgets.launcher')

-- Bar :
local function get_bar(s)
    -- Create the wibox
	s.mywibar = awful.wibar({
		position = "bottom",
		type = "dock",
		ontop = false,
		stretch = true,
		visible = true,
		height = dpi(42),
		width = s.geometry.width,
		screen = s,
		bg = colors.black,
		--bg = colors.main_transparent,
		--bg = colors.transparent,
		--opacity = 0.85,
	})
 
	--s.mywibar:struts { bottom = dpi(60), top = dpi(20), left = dpi(20), right = dpi(20) }
 
    -- Bar setup :
    s.mywibar:setup {
		{
			{
				{
					{
						launcher,
						taglist(s),
						--tasklist(s),
						spacing = dpi(10),
						layout = wibox.layout.fixed.horizontal
					},
					--nil,
					-- # Tasks in middel :
					{
						tasklist(s),
						layout = wibox.layout.fixed.horizontal
					},
					{
						keyboard_widget,
						clock_widget,
						systray,
						sidebar_button,
						mylayoutbox,
						layout = wibox.layout.fixed.horizontal,
						spacing = dpi(10)
					},
					layout = wibox.layout.align.horizontal,
					expand = "none"
				},
				widget = wibox.container.margin,
				margins = {left = dpi(15), right = dpi(15), top = dpi(2), bottom = dpi(2)}
			},
			widget = wibox.container.background,
			bg = colors.bg_color,
			forced_height = s.mywibar.height
		},
		layout = wibox.layout.fixed.vertical,
		spacing = dpi(10)
    }  
    
    
    -- function to remove the bar in maxmized/fullscreen apps
	local function remove_wibar(c)
		if c.fullscreen or c.maximized then
			c.screen.mywibar.visible = false
		else
			c.screen.mywibar.visible = true
		end
    end

    local function add_wibar(c)
        if c.fullscreen or c.maximized then
            c.screen.mywibar.visible = true
        end
    end
    client.connect_signal("property::fullscreen", remove_wibar)
    client.connect_signal("request::unmanage", add_wibar)
 
end

screen.connect_signal("request::desktop_decoration", function(s)
	get_bar(s)
end)

