-- ## Sliders ##
-- ~~~~~~~~~~~~~

-- Requirements :
-- ~~~~~~~~~~~~~~
local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require ("beautiful")
local dpi = beautiful.xresources.apply_dpi

-- Helper
-----------
local function round_widget(radius)
      	return function(cr,w,h)
       	 	gears.shape.rounded_rect(cr,w,h,radius)
       end
end

-- Create volume slider
--local volume_icon = wibox.widget.textbox()
--volume_icon.font = theme.sidebar_font
--volume_icon.align = 'left'
--volume_icon.markup = ""
--volume_icon.fg = colors.blue
local v_icon = wibox.widget {
	markup = '<span font="' .. theme.sidebar_font .. '"></span>',
	widget = wibox.widget.textbox,
}
local volume_icon =	wibox.widget{
		v_icon,
		fg = colors.blue,
		widget = wibox.container.background
}
local volume_osd_value = wibox.widget({
	text = "0%",
	font = theme.font,
	align = "center",
	valign = "center",
	widget = wibox.widget.textbox,
})

local volume_slider = wibox.widget {
	forced_width = dpi(220),
	forced_height = dpi(10),
	bar_shape = round_widget(12),
	bar_height = dpi(14),
	bar_color = colors.black,
	bar_active_color = colors.blue,
	handle_shape = gears.shape.circle,
	handle_color = colors.brightblue,
	handle_width = dpi(20),
	widget = wibox.widget.slider
}

volume = wibox.widget {
	nil,
	{
		volume_icon,
		volume_slider,
		volume_osd_value,
		spacing = dpi(20),
		layout = wibox.layout.fixed.horizontal,
	},
	expand = "none",
	layout = wibox.layout.align.vertical,
}

local update_volume = function() 
	awful.spawn.easy_async_with_shell("pamixer --get-volume", function(stdout) 
		volume_slider.value = tonumber(stdout:match("%d+"))
	end)
end

volume_slider:connect_signal("property::value", function(_, vol) 
	awful.spawn("pamixer --set-volume ".. vol, false)
	-- Update textbox widget text
	volume_osd_value.text = vol .. "%"
	awesome.emit_signal("module::volume_osd_value", vol)
end)

-- Create Mic slider
local m_icon = wibox.widget {
	markup = '<span font="' .. theme.sidebar_font .. '">  </span>',
	widget = wibox.widget.textbox,
}
local mic_icon = wibox.widget{
		m_icon,
		fg = colors.green,
		widget = wibox.container.background
}
local mic_osd_value = wibox.widget({
	text = "0%",
	font = theme.font,
	align = "center",
	valign = "center",
	widget = wibox.widget.textbox,
})

local mic_slider = wibox.widget {
	forced_width = dpi(220),
	forced_height = dpi(10),
	bar_shape = round_widget(12),
	bar_height = dpi(14),
	bar_color = colors.black,
	bar_active_color = colors.green,
	handle_shape = gears.shape.circle,
	handle_color = colors.brightgreen,
	handle_width = dpi(20),
	widget = wibox.widget.slider
}

local mic = wibox.widget {
	nil,
	{
		mic_icon,
		mic_slider,
		mic_osd_value,
		spacing = dpi(20),
		layout = wibox.layout.fixed.horizontal,
	},
	expand = "none",
	layout = wibox.layout.align.vertical,
}

local update_mic = function() 
	awful.spawn.easy_async_with_shell("pamixer --source alsa_input.usb-1c1f_USB_PnP_Audio_Device-00.mono-fallback --get-volume", function(stdout) 
		mic_slider.value = tonumber(stdout:match("%d+"))
	end)
end

mic_slider:connect_signal("property::value", function(_, mic_vol) 
	awful.spawn("pamixer --source alsa_input.usb-1c1f_USB_PnP_Audio_Device-00.mono-fallback	--set-volume ".. mic_vol, false)
	-- Update textbox widget text
	mic_osd_value.text = mic_vol .. "%"
	awesome.emit_signal("module::mic_osd_value", mic_vol)
end)

-- Create brightness slider
--local bright_icon = wibox.widget.textbox()
--bright_icon.font = theme.sidebar_font
--bright_icon.align = 'left'
--bright_icon.markup = " "
local b_icon = wibox.widget {
	markup = '<span font="' .. theme.sidebar_font .. '"> </span>',
	widget = wibox.widget.textbox,
}
local bright_icon =	wibox.widget{
		b_icon,
		fg = colors.yellow,
		widget = wibox.container.background
}
local bright_osd_value = wibox.widget({
	text = "0%",
	font = theme.font,
	align = "center",
	valign = "center",
	widget = wibox.widget.textbox,
})

local bright_slider = wibox.widget {
	forced_width = dpi(220),
	forced_height = dpi(30),
	bar_shape = round_widget(12),
	bar_height = dpi(14),
	bar_color = colors.black,
	bar_active_color = colors.yellow,
	handle_shape = gears.shape.circle,
	handle_color = colors.brightyellow,
	handle_width = dpi(20),
	widget = wibox.widget.slider
}

bright_slider:connect_signal("property::value", function(_, bri) 
	awful.spawn("brightnessctl set "..bri.."%", false)
	bright_osd_value.text = bri .. "%"
	awesome.emit_signal("module::bright_osd_value", bri)
end)

local update_brightness = function()
        awful.spawn.easy_async_with_shell("brightnessctl g", function(stdout)
		val = tonumber(string.format("%.0f", stdout))
			awful.spawn.easy_async_with_shell("brightnessctl max", function(stdout)
				bri = val/tonumber(string.format("%.0f", stdout)) * 100
				bri = tonumber(string.format("%.0f", bri)) or 0
				bright_slider.value = bri
			end)
        end)
end

local brightness = wibox.widget {
        nil,
        {
			bright_icon,
			bright_slider,
			bright_osd_value,
			spacing = dpi(20),
			layout = wibox.layout.fixed.horizontal,
        },
        expand = "none",
        layout = wibox.layout.align.vertical,
}

gears.timer {
	timeout = 10,
	autostart = true,
	call_now = true,
	callback = function() 
		update_volume()
		update_mic()
		update_brightness()
	end
}

local all_slider = wibox.widget {
	volume,
	mic,
	brightness,
	spacing = dpi(10),
	layout = wibox.layout.fixed.vertical,
}

return all_slider
