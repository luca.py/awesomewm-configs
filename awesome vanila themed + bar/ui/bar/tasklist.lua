-- ## Tasklist ##
-- ~~~~~~~~~~~~~~

-- requirements
-- ~~~~~~~~~~~~
local gears = require("gears")
local awful = require("awful")



return function(s)

    -- Tasklist buttons
    local tasklist_buttons = gears.table.join(
		awful.button({ }, 1, function (c) c:activate { context = "tasklist", action = "toggle_minimization" } end),
		awful.button({ }, 3, function() awful.menu.client_list { theme = { width = 250 } } end),
		awful.button({ }, 4, function() awful.client.focus.byidx(-1) end),
		awful.button({ }, 5, function() awful.client.focus.byidx( 1) end)
    )

	-- Create a tasklist widget
    local tasklist = awful.widget.tasklist 
	{
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons,
	}
	return tasklist
end
