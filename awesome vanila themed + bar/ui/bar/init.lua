-- ## Bar ##
-- ~~~~~~~~~

-- Requirements :
-- ~~~~~~~~~~~~~~
local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

-- # Bar Widgets :
-- ~~~~~~~~~~~~~~~
local taglist = require("ui.bar.taglist")
local tasklist = require("ui.bar.tasklist")
local layoutbox = require("ui.bar.layoutbox")
mylayoutbox = wibox.container.margin(layoutbox(s))


-- # Widgets :
-- ~~~~~~~~~~~
-- # Keyboard : 
mykeyboardlayout = awful.widget.keyboardlayout()
-- # Clock : 
mytextclock = wibox.widget.textclock()




local function get_bar(s)
    -- Create the wibox
	s.mywibar = awful.wibar({
		position = "bottom",
		type = "tooltip",
		ontop = false,
		stretch = false,
		visible = true,
		height = dpi(42),
		width = s.geometry.width,
		screen = s,
		bg = colors.black,
		--bg = colors.main_transparent,
		--bg = colors.transparent,
		--opacity = 0.85,
	})
 
	--s.mywibar:struts { bottom = dpi(60), top = dpi(20), left = dpi(20), right = dpi(20) }
 
    -- Bar setup :
    s.mywibar:setup {
		{
			{
				{
					{
						mylauncher,
						taglist(s),
						tasklist(s),
						spacing = dpi(15),
						layout = wibox.layout.fixed.horizontal
					},
					nil,
					{
						mytextclock,
						wibox.widget.systray,
						layoutbox(s),
						layout = wibox.layout.fixed.horizontal,
						spacing = dpi(15)
					},
					layout = wibox.layout.align.horizontal,
					expand = "none"
				},
				widget = wibox.container.margin,
				margins = {left = dpi(15), right = dpi(15), top = dpi(8), bottom = dpi(8)}
			},
			widget = wibox.container.background,
			bg = colors.bg_color,
			forced_height = s.mywibar.height
		},
		layout = wibox.layout.fixed.vertical,
		spacing = dpi(0)
    }  
    
    
    -- function to remove the bar in maxmized/fullscreen apps
	local function remove_wibar(c)
		if c.fullscreen or c.maximized then
			c.screen.mywibar.visible = false
		else
			c.screen.mywibar.visible = true
		end
    end

    local function add_wibar(c)
        if c.fullscreen or c.maximized then
            c.screen.mywibar.visible = true
        end
    end
    client.connect_signal("property::fullscreen", remove_wibar)
    client.connect_signal("request::unmanage", add_wibar)
 
end

screen.connect_signal("request::desktop_decoration", function(s)
	get_bar(s)
end)

