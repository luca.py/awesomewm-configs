-- Colors :
colors = {}
-- Dark colors
colors.black 									= "#262626"
colors.red 										= "#cc241d"
colors.green 									= "#98971a"
colors.yellow 									= "#d79921"
colors.blue 									= "#458588"
colors.magenta 									= "#b16286"
colors.cyan 									= "#689d6a"
colors.white 									= "#acacac"
-- Bright colors
colors.brightblack 								= "#333333"
colors.brightred 								= "#fb4934"
colors.brightgreen 								= "#b8bb26"
colors.brightyellow 							= "#fabd2f"
colors.brightblue 								= "#83a598"
colors.brightmagenta 							= "#d3869b"
colors.brightcyan 								= "#8ec07c"
colors.brightwhite 								= "#dfdfdf"
-- Other
colors.transparent 								= "#00000000"
colors.container 								= "#303030"
colors.main_scheme								= "#458588"
colors.main_transparent 						= "#262626CC"

return colors
