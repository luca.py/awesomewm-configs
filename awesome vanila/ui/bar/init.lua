-- ## Bar ##
-- ~~~~~~~~~

-- requirements
-- ~~~~~~~~~~~~
local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

-- # Bar Widgets :
local taglist = require("ui.bar.taglist")
local tasklist = require("ui.bar.tasklist")
local layoutbox = require("ui.bar.layoutbox")
mylayoutbox = wibox.container.margin(layoutbox(s))


-- # Widgets :

-- # Keyboard : 
mykeyboardlayout = awful.widget.keyboardlayout()
-- # Clock : 
mytextclock = wibox.widget.textclock()


local function get_bar(s)
    -- Create the wibox
    s.mywibar = awful.wibar
    ({
        position = "bottom",
        screen   = s,
    })
    
	s.mywibar:setup
	({
		{
			layout = wibox.layout.align.horizontal,
			--expand = "none",
			{ -- Left widgets :
				layout = wibox.layout.fixed.horizontal,
				mylauncher,
				taglist(s),
			},
			{ -- Middle widget :
				tasklist(s),
				layout = wibox.layout.align.horizontal,
			},
			--tasklist(s),
			{ -- Right widgets :
				layout = wibox.layout.fixed.horizontal,
				mykeyboardlayout,
				wibox.widget.systray(),
				mytextclock,
				mylayoutbox,
			},
		},
		shape  = function(cr,w,h) gears.shape.rounded_rect(cr,w,h,0) end,
		widget = wibox.container.background,
	})
   
end

screen.connect_signal("request::desktop_decoration", function(s)
	get_bar(s)
end)

