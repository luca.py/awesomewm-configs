-- ## Notifications ##
-- ~~~~~~~~~~~~~~~~~~~

-- Requirements :
-- ~~~~~~~~~~~~~~
local awful = require "awful"
local wibox = require "wibox"
local gears = require "gears" 
local beautiful = require "beautiful"
local naughty = require "naughty"
local dpi = beautiful.xresources.apply_dpi


-- # Error handling :
naughty.connect_signal("request::display_error", function(message, startup)
    naughty.notification {
        urgency = "critical",
        title   = "Oops, an error happened"..(startup and " during startup!" or "!"),
        message = message
    }
end)

naughty.connect_signal("request::display", function(n) 
	create_notifcation(n)
end)
