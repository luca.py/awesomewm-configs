-- ## Tag ##
-- ~~~~~~~~~

-- Requirements :
-- ~~~~~~~~~~~~~~
local awful = require("awful")
local gears = require("gears")


-- Tags :
--awful.util.tagnames =  { "1", "2", "3", "4", "5", "6", "7", "8", "9" }
awful.util.tagnames =  { "", "", "", "", "", "", "", "", ""}

-- Each screen has its own tag table.
screen.connect_signal("request::desktop_decoration", function(s)
	awful.tag(awful.util.tagnames, s, awful.layout.layouts[1])
end)


return function(s)

    -- Taglist buttons
    local taglist_buttons = gears.table.join(
		awful.button({ }, 1, function(t) t:view_only() end),
		awful.button({ modkey }, 1, function(t)
			if client.focus then
				client.focus:move_to_tag(t)
			end
		end),

		awful.button({ }, 3, awful.tag.viewtoggle),
		awful.button({ modkey }, 3, function(t)
			if client.focus then
				client.focus:toggle_tag(t)
			end
		end)

		--awful.button({ }, 4, function(t) awful.tag.viewprev(t.screen) end),
		--awful.button({ }, 5, function(t) awful.tag.viewnext(t.screen) end),
	)

	-- Create a taglist widget
	local taglist = awful.widget.taglist {
		screen  = s,
		--filter  = awful.widget.taglist.filter.all,
		filter = function (t) return t.selected or #t:clients() > 0 end, -- Show only used Tags
		buttons = taglist_buttons
	}
	
	return taglist
end
