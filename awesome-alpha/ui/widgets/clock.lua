-- ## Clock ##
-- ~~~~~~~~~~~~~

-- Requirements :
-- ~~~~~~~~~~~~~~
local wibox = require('wibox')
local beautiful = require('beautiful')
local dpi = require('beautiful').xresources.apply_dpi
local dashboard = require("ui.dashboard")

-- # Libs :
-- ~~~~~~~~
local helpers = require("libs.helpers")

-- Clock :
--local clock = wibox.widget.textclock('<span font="' .. theme.font .. '">%a %d %b | %H:%M</span>')
local clock = wibox.widget.textclock('%H : %M')

-- Icon :
local widget_icon = " "
local icon = wibox.widget{
    font   	= theme.icon_font,
    markup 	= helpers.colorize_text(widget_icon, colors.main_scheme),
    widget 	= wibox.widget.textbox,
    valign 	= "center",
    align 	= "center"
}

-- press
clock:connect_signal(
    "button::press",
    function()
        clock.opacity = 0.6
        dd_toggle()
end)
clock:connect_signal(
    "button::release",
    function()
        clock.opacity = 1
end)



-- update ind status
--------------------
--awesome.connect_signal("dashboard::visible", function(val) 
--    if val then 
--        indicator.visible = true
--    else
--        indicator.visible = false
--    end
--end)


return wibox.widget {
	icon,
    wibox.widget{
        clock, 
        fg = colors.brightwhite,
        widget = wibox.container.background
    },
    spacing = dpi(2),
    layout = wibox.layout.fixed.horizontal
}
