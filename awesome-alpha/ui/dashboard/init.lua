-- ## dashboard ##
-- ~~~~~~~~~~~~~~~~

-- Requirements :
-- ~~~~~~~~~~~~~~
local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require ("beautiful")
local dpi = beautiful.xresources.apply_dpi

-- # Libs :
-- ~~~~~~~~
local helpers = require("libs.helpers")

-- widgets
----------
local notifs = require("ui.dashboard.notifs.build")

awful.screen.connect_for_each_screen(function(s)

    local screen_height = s.geometry.height

    -- Mainbox
    ---------------------
    dashbaord_d = wibox({
        type = "dock",
        screen = s,
        width = dpi(430),
        height = screen_height - (dpi(42) + theme.useless_gap * 4),
        shape = helpers.rrect(theme.rounded),
        bg = colors.black,
        ontop = true,
        visible = false
    })


    dd_toggle = function(s) 
        --control_hide()
        if not dashbaord_d.visible then
            dashbaord_d.visible = true
            awesome.emit_signal("dashboard::visible", true)
        else
            dashbaord_d.visible = false
            awesome.emit_signal("dashboard::visible", false)
        end
    end
	awful.placement.top_right(dashbaord_d, {honor_workarea = true, margins = beautiful.useless_gap * 2})
    --~~~~~~~~~~~~~~~

    dashbaord_d:setup {
        {
            {
                notifs,
                widget = wibox.container.background,
                bg = colors.black,
                shape = helpers.rrect(beautiful.rounded)
            },
            margins = dpi(15),
            widget = wibox.container.margin
        },
        layout = wibox.layout.align.vertical
    }

end)
