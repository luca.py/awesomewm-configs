-- ## Clear Button ##
-- ~~~~~~~~~~~~~~~~~~

-- Requirements :
-- ~~~~~~~~~~~~~~
local wibox = require("wibox")
local naughty = require("naughty")
local beautiful = require ("beautiful")
local dpi = beautiful.xresources.apply_dpi

-- # Libs :
-- ~~~~~~~~
local helpers = require("libs.helpers")



-- clear button
return require("libs.helpers.widgets.create_button")(
  {
    markup = helpers.colorize_text("Clear All", (colors.brightwhite or colors.white)),
    font = theme.font,
    align = "center",
    valign = "center",
    widget = wibox.widget.textbox,
  },
  colors.black,
  colors.white,
  {top = dpi(6), bottom = dpi(6), left = dpi(14), right = dpi(14)},
  dpi(0),
  dpi(0),
  helpers.rrect(theme.rounded)
)
