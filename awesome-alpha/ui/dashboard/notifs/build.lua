-- ## Build ##
-- ~~~~~~~~~~~

-- Requirements :
-- ~~~~~~~~~~~~~~
local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local naughty = require("naughty")
local beautiful = require ("beautiful")
local dpi = beautiful.xresources.apply_dpi

-- # Libs :
-- ~~~~~~~~
local helpers = require("libs.helpers")

-- clear button
-------------------------------------------------------------------
local notifs_clear = require("ui.dashboard.notifs.clear_button")

notifs_clear:buttons(gears.table.join(awful.button({}, 1, function()
  _G.notif_center_reset_notifs_container()
end)))
--------------------------------------------------------------------

-- empty box
-------------------------------------------------------------------
local notifs_empty = require("ui.dashboard.notifs.notifs-empty")
--------------------------------------------------------------------

-- Create notif
--------------------------------------------------------------
local create_notif = require("ui.dashboard.notifs.creator")
---------------------------------------------------------------

-- Notifs container
--------------------------------------------------------------------
local notifs_container = require("ui.dashboard.notifs.container")
--------------------------------------------------------------------

-- Helper functions
-------------------------------
local remove_notifs_empty = true

notif_center_reset_notifs_container = function()
  notifs_container:reset(notifs_container)
  notifs_container:insert(1, notifs_empty)
  remove_notifs_empty = true
end

notif_center_remove_notif = function(box)
  notifs_container:remove_widgets(box)

  if #notifs_container.children == 0 then
    notifs_container:insert(1, notifs_empty)
    remove_notifs_empty = true
  end
end
--------------------------------------------------

-- update functions
----------------------------------------
notifs_container:insert(1, notifs_empty)

naughty.connect_signal("request::display", function(n)
  if #notifs_container.children == 1 and remove_notifs_empty then
    notifs_container:reset(notifs_container)
    remove_notifs_empty = false
  end

  local appicon = n.icon or n.app_icon
  if not appicon then
    appicon = theme.notification_icon
  end

  notifs_container:insert(1, create_notif(appicon, n))
end)
-------------------------------------------------------

-----------------------------------------------
-----------------------------------------------
return wibox.widget {
    {
      {
          {
              {
                  widget = wibox.widget.textbox,
                  markup = helpers.colorize_text("Notifications", colors.white),
                  font = theme.font
              },
              nil,
              notifs_clear,
              layout = wibox.layout.align.horizontal
          },
          {
              notifs_container,
              nil,
              layout = wibox.layout.align.vertical,
              expand = "none"
          },
          layout = wibox.layout.fixed.vertical,
          spacing = dpi(20)
      },
        margins = dpi(20),
        widget = wibox.container.margin
    },
    widget = wibox.container.background,
    bg = colors.black,
    shape = helpers.rrect(theme.rounded)
}
-----------------------------------------------
-----------------------------------------------
