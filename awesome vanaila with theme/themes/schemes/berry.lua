-- Colors :
colors = {}
-- Dark colors
colors.black 									= "#1C2026"
colors.red 										= "#dc6f7a"
colors.green 									= "#618774"
colors.yellow 									= "#e3786c"
colors.blue 									= "#4f6286"
colors.magenta 									= "#a97594"
colors.cyan 									= "#4f8896"
colors.white 									= "#E8E8EA"
-- Bright colors
colors.brightblack 								= "#2F3640"
colors.brightred 								= "#e38588"
colors.brightgreen 								= "#7eab7b"
colors.brightyellow 							= "#ee896e"
colors.brightblue 								= "#5c6b92"
colors.brightmagenta 							= "#b589a4"
colors.brightcyan 								= "#6ba5ab"
colors.brightwhite 								= "#E7E8EA"
-- Other
colors.transparent 								= "#00000000"
colors.container 								= "#22272E"
colors.main_scheme								= "#9D6D89"
colors.main_transparent 						= "#1E2329CC"

return colors
