-- ## Bar ##
-- ~~~~~~~~~

-- requirements
-- ~~~~~~~~~~~~
local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi


-- # Bar Widgets :
local taglist = require("ui.bar.taglist")
local tasklist = require("ui.bar.tasklist")
local layoutbox = require("ui.bar.layoutbox")
mylayoutbox = wibox.container.margin(layoutbox(s), dpi(4), dpi(4), dpi(4), dpi(4))


-- # Widgets :
local clock_widget = require('ui.widgets.clock')
local keyboardlayout_widget = require('ui.widgets.keyboardlayout')
local mem_widget = require('ui.widgets.memory')
local cpu_widget = require('ui.widgets.cpu')
local temprature_widget = require('ui.widgets.temprature')
local battery_widget = require('ui.widgets.battery')
local brightness_widget = require('ui.widgets.brightness')
local netspeed_widget = require('ui.widgets.net')
local logo_widget = require('ui.widgets.logo')
mylauncher = wibox.container.margin(mylauncher, dpi(4), dpi(4), dpi(4), dpi(4))


-- Barcontainer :
local function barcontainer(widget)
    local container = wibox.widget
      {
        widget,
        top = dpi(0),
        bottom = dpi(0),
        left = dpi(2),
        right = dpi(2),
        widget = wibox.container.margin
    }
    local box = wibox.widget{
        {
            container,
            top = dpi(2),
            bottom = dpi(2),
            left = dpi(10),
            right = dpi(10),
            widget = wibox.container.margin
        },
        bg = colors.container,
        shape = function(cr,w,h) gears.shape.rounded_rect(cr,w,h,0) end,
        --shape = function(cr,w,h) gears.shape.rounded_bar(cr,w,h,20) end,
        widget = wibox.container.background
    }
return wibox.widget{
        box,
        top = dpi(2),
        bottom = dpi(2),
        right = dpi(2),
        left = dpi(2),
        widget = wibox.container.margin
    }
end


-- Separator :
local separator = wibox.widget{
    markup = '<span font="' .. theme.font .. '">  </span>',
    align  = 'center',
    valign = 'center',
    widget = wibox.widget.textbox
}

-- Systray
local systray = wibox.widget {
	visible = true,
	base_size = dpi(30),
	horizontal = true,
	screen = 'primary',
	{
		{
			{
				wibox.widget.systray,
				layout = wibox.layout.fixed.horizontal,
			},
			margins = {top = dpi(4), bottom = dpi(4), left = dpi(4), right = dpi(4)},
			widget = wibox.container.margin,
		},
		shape = function(cr,w,h) gears.shape.rounded_rect(cr,w,h,0) end,
		bg = colors.black,
		widget = wibox.container.background,
	},
	margins = {top = dpi(2), bottom = dpi(2)},
	widget = wibox.container.margin,
}


-- Right
local right = wibox.widget {
	{	
		-- # Updates :
		barcontainer(netspeed_widget),
		-- # Brightness :
		--barcontainer(brightness_widget),
		-- # CPU TEMP :
		barcontainer(temprature_widget),
		-- # CPU :
		barcontainer(cpu_widget),
		-- # RAM :
		barcontainer(mem_widget),
		-- # Keybord :
		barcontainer(keyboardlayout_widget),
		-- # Clock :
		barcontainer(clock_widget),	
		-- # Systry :
		--systray,
		-- # Logout :
		--logout_menu,
		-- # Layoutbox :
		--layoutbox(s),
		spacing = dpi(4),
		layout = wibox.layout.fixed.horizontal,
	},
	margins = {top = dpi(2), bottom = dpi(2)},
	widget = wibox.container.margin,
}

logo = wibox.widget{
	{
		logo_widget,
		top = dpi(0),
		bottom = dpi(0),
		left = dpi(8),
		right = dpi(2),
		widget = wibox.container.margin
	},
	bg = colors.black,
	fg = colors.main_scheme,
	--shape = function(cr,w,h) gears.shape.rounded_rect(cr,w,h,0) end,
	--shape = function(cr,w,h) gears.shape.powerline(cr,w,h) end,
	--shape = function(cr,w,h) gears.shape.hexagon(cr,w,h,20) end,
	--shape = function(cr,w,h) gears.shape.rounded_bar(cr,w,h,-20) end,
	widget = wibox.container.background
}

local function get_bar(s)
	s.mywibar = awful.wibar({
		position = "bottom",
		type = "dock",
		ontop = false,
		stretch = false,
		visible = true,
		height = dpi(36),
		width = s.geometry.width,
		screen = s,
		bg = colors.black,
		--bg = colors.main_transparent,
		--bg = colors.transparent,
		--opacity = 0.85,
	})
	
	tags = wibox.widget{
		{
			taglist(s),
			top = dpi(0),
			bottom = dpi(0),
			left = dpi(12),
			right = dpi(12),
			widget = wibox.container.margin
		},
		bg = colors.container,
		--shape = function(cr,w,h) gears.shape.rounded_rect(cr,w,h,0) end,
		--shape = function(cr,w,h) gears.shape.powerline(cr,w,h,20) end,
		--shape = function(cr,w,h) gears.shape.hexagon(cr,w,h,20) end,
		shape = function(cr,w,h) gears.shape.rounded_bar(cr,w,h,-20) end,
		widget = wibox.container.background
	}

	--s.mywibar:struts { bottom = dpi(60), top = dpi(36), left = dpi(20), right = dpi(20) }
	
	s.mywibar:setup({
		{
			{
				layout = wibox.layout.align.horizontal,
				--expand = "none",
				{ -- Left widgets :
					--mylauncher,
					logo,
					taglist(s),	
					--tags,
					separator,
					spacing = dpi(8),
					layout = wibox.layout.fixed.horizontal,
				},
				{ -- Middle widget :
					tasklist(s),
					layout = wibox.layout.align.horizontal,
				},
				--tasklist(s),
				{ -- Right widgets :
					right,
					-- # Systry :
					systray,
					-- # Layoutbox :
					mylayoutbox,
					layout = wibox.layout.fixed.horizontal,
				},
			},
			left = dpi(-2),
			right = dpi(20),
			widget = wibox.container.margin,
		},
		shape  = function(cr,w,h) gears.shape.rounded_rect(cr,w,h,0) end,
		widget = wibox.container.background,
	})
end

screen.connect_signal("request::desktop_decoration", function(s)
	get_bar(s)
end)
