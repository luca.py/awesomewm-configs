-- ## Tasklist ##
-- ~~~~~~~~~~~~~~

-- requirements
-- ~~~~~~~~~~~~
local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi


return function(s)
	-- Create a tasklist widget
    local tasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = {
            awful.button({ }, 1, function (c)
                c:activate { context = "tasklist", action = "toggle_minimization" }
            end),
            awful.button({ }, 3, function() awful.menu.client_list { theme = { width = 250 } } end),
            awful.button({ }, 4, function() awful.client.focus.byidx(-1) end),
            awful.button({ }, 5, function() awful.client.focus.byidx( 1) end),
        },
		style    = {
			border_width = 0,
			border_color = colors.black,
			-- Text Enabeld :
			shape = function(cr,w,h) gears.shape.rounded_rect(cr,w,h,0) end,
			-- Text disabeld :
			--shape        = gears.shape.circle,
			bg_minimize = colors.black,
			bg_normal 	= colors.container,
			bg_focus	= colors.container,
		},		
		layout   = {
			spacing = dpi(4),
			spacing_widget = {
				{
					forced_width = 0,
					shape        = gears.shape.circle,
					widget       = wibox.widget.separator
				},
				valign = "center",
				halign = "center",
				widget = wibox.container.place,
			},
			layout  = wibox.layout.flex.horizontal
		},
		widget_template = {
			{
				{
					{
						{
							-- Icon :
							{
								id     = "clienticon",
								widget = awful.widget.clienticon,
							},
							margins = 2,
							widget  = wibox.container.margin,
						},
						-- Text :
						--{
						--	id     = "text_role",
						--	widget = wibox.widget.textbox,
						--},
						layout = wibox.layout.fixed.horizontal,
					},
					top = dpi(3),
					bottom = dpi(3),
					left = dpi(5),
					right = dpi(5),
					widget = wibox.container.margin
				},
				id     = "background_role",
				widget = wibox.container.background,
			},
			top = dpi(0),
			bottom = dpi(0),
			left = dpi(4),
			right = dpi(4),
			widget  = wibox.container.margin
		}
    }
	return tasklist
end
