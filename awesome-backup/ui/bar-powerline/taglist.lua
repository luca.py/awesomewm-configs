-- ## Tag ##
-- ~~~~~~~~~

-- requirements
-- ~~~~~~~~~~~~
local awful = require("awful")


-- Tags :
awful.util.tagnames =  { "", "", "", "", "", "", "", "", ""}

-- Each screen has its own tag table.
screen.connect_signal("request::desktop_decoration", function(s)
	awful.tag(awful.util.tagnames, s, awful.layout.layouts[1])
end)

return function(s)
    -- Create a taglist widget
    local taglist = awful.widget.taglist {
        screen  = s,
        --filter  = awful.widget.taglist.filter.all, -- Show All Tags
        filter = function (t) return t.selected or #t:clients() > 0 end, -- Show only used Tags
        buttons = {
            awful.button({ }, 1, function(t) t:view_only() end),
            awful.button({ modkey }, 1, function(t)
				if client.focus then
					client.focus:move_to_tag(t)
				end
			end),
            awful.button({ }, 3, awful.tag.viewtoggle),
            awful.button({ modkey }, 3, function(t)
				if client.focus then
					client.focus:toggle_tag(t)
				end
			end),
            --awful.button({ }, 4, function(t) awful.tag.viewprev(t.screen) end),
            --awful.button({ }, 5, function(t) awful.tag.viewnext(t.screen) end),
        }
	}
	return taglist
end
