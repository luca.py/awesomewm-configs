-- ## redshift ##
-- ~~~~~~~~~~~~~~

-- requirements
-- ~~~~~~~~~~~~
local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require ("beautiful")
local dpi = beautiful.xresources.apply_dpi

local toggle = wibox.widget.textbox()
toggle.font = theme.sidebar_font

--toggle:buttons(gears.table.join(
--	awful.button({}, 1, function() 
--		awful.spawn("redshift -l 0:0 -t 4500:4500 -r &>/dev/null & echo 'ON'", function(stdout)) 
--		if stdout:match("ON") then
--			toggle.markup = ""
--		else
--			toggle.markup = ""
--		end
--	end)
--))

toggle:buttons(gears.table.join(
		awful.spawn.easy_async_with_shell(
			[[
			if [ ! -z $(pgrep redshift) ];
			then
				redshift -x && pkill redshift && killall redshift
				echo 'OFF'
			else
				redshift -l 0:0 -t 4500:4500 -r &>/dev/null &
				echo 'ON'
			fi
			]],
			function(stdout)
				if stdout:match("ON") then
					toggle.markup = ""
				else
					toggle.markup = ""
				end
			end
		),
	),
)

local buttons = wibox.widget {
	toggle,
	spacing = dpi(8),
	layout = wibox.layout.fixed.horizontal,
}

return wibox.widget {
	{
		nil,
		buttons,
		expand = 'none',
		layout = wibox.layout.align.horizontal,
	},
	spacing = dpi(12),
	layout = wibox.layout.fixed.vertical,
}

