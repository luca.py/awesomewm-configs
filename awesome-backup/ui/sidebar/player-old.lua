-- ## Minimal music widget ##
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~

-- Requirements :
-- ~~~~~~~~~~~~~~
local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require ("beautiful")
local dpi = beautiful.xresources.apply_dpi


-- # Libs :
-- ~~~~~~~~
local helpers = require("libs.helpers")
local playerctl = require("libs.bling").signal.playerctl.lib()

-- # Widgets
-- # ~~~~~~~
-- Song info
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-- album art
local album_art = wibox.widget{
    widget = wibox.widget.imagebox,
    clip_shape = helpers.rrect(theme.rounded),
    forced_height = dpi(85),
    forced_width = dpi(85),
    image = theme.album_art
}


-- song artist
local song_artist = wibox.widget{
    widget = wibox.widget.textbox,
    markup = helpers.colorize_text("Unknown", colors.white),
    font = theme.font,
    align = "left",
    valign = "center"
}

-- song name
local song_name = wibox.widget{
    widget = wibox.widget.textbox,
    markup = helpers.colorize_text("None", colors.white),
    font = theme.font,
    align = "left",
    valign = "center"
}

-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ EOF Song info



-- buttons
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-- toggle button
local toggle_button = wibox.widget{
    widget = wibox.widget.textbox,
    markup = helpers.colorize_text("", colors.white),
    font = theme.sidebar_font,
    align = "right",
    valign = "center"
}

-- next button
local next_button = wibox.widget{
    widget = wibox.widget.textbox,
    markup = helpers.colorize_text("", colors.white),
    font = theme.sidebar_font,
    align = "right",
    valign = "center"
}

-- prev button
local prev_button = wibox.widget{
    widget = wibox.widget.textbox,
    markup = helpers.colorize_text("", colors.white),
    font = theme.sidebar_font,
    align = "right",
    valign = "center"
}

-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ EOF buttons


-- update widgets
-- ~~~~~~~~~~~~~~
local toggle_command = function() playerctl:play_pause() end
local prev_command = function() playerctl:previous() end
local next_command = function() playerctl:next() end

toggle_button:buttons(gears.table.join(
    awful.button({}, 1, function() toggle_command() end)))

next_button:buttons(gears.table.join(
    awful.button({}, 1, function() next_command() end)))

prev_button:buttons(gears.table.join(
    awful.button({}, 1, function() prev_command() end)))


playerctl:connect_signal("metadata", function(_, title, artist, album_path, __, ___, ____)
	if title == "" then
		title = "None"
	end
	if artist == "" then
		artist = "Unknown"
	end
	if album_path == "" then
		album_path = theme.album_art
	end

	album_art:set_image(gears.surface.load_uncached(album_path))
    song_name:set_markup_silently(helpers.colorize_text(title, colors.white))
	song_artist:set_markup_silently(helpers.colorize_text(artist, colors.white))


end)

playerctl:connect_signal("playback_status", function(_, playing, __)
	if playing then
        toggle_button.markup = helpers.colorize_text("", colors.white)
	else
        toggle_button.markup = helpers.colorize_text("", colors.white)
	end
end)

-- ~~~~~~~~~~~~~~~~~~
-- ~~~~~~~~~~~~~~~~~~
return wibox.widget {
    {
        {
            album_art,
            {
                {
                    nil,
                    {
                        {
                            step_function = wibox.container.scroll.step_functions.waiting_nonlinear_back_and_forth,
                            widget = wibox.container.scroll.horizontal,
                            forced_width = dpi(158),
                            speed = 30,
                            song_name,
                        },
                        {
                            step_function = wibox.container.scroll.step_functions.waiting_nonlinear_back_and_forth,
                            widget = wibox.container.scroll.horizontal,
                            forced_width = dpi(158),
                            speed = 30,
                            song_artist,
                        },
                        spacing = dpi(10),
                        layout = wibox.layout.fixed.vertical,
                    },
                    layout = wibox.layout.align.vertical,
                    expand = "none"
                },
                {
                    prev_button,
                    toggle_button,
                    next_button,
                    layout = wibox.layout.fixed.horizontal,
                    spacing = dpi(8)
                },
                layout = wibox.layout.fixed.horizontal,
                spacing = dpi(10)
            },
            layout = wibox.layout.align.horizontal,
            spacing = dpi(10)
        },
        --margins = dpi(20),
        margins = {top = dpi(4), bottom = dpi(4), left = dpi(0), right = dpi(0)},
        widget = wibox.container.margin
    },
    widget = wibox.container.background,
    forced_height = dpi(110),
    bg = colors.brightblack,
    border_color = colors.brightblack,
    shape = helpers.rrect(theme.rounded)
}
-- ~~~~~~~~~~~~~~~~~~
