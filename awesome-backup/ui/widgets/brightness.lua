-- ## Brightness ##
-- ~~~~~~~~~~~~~

-- requirements
-- ~~~~~~~~~~~~
local wibox = require('wibox')
local watch = require('awful.widget.watch')
local beautiful = require('beautiful')
local dpi = require('beautiful').xresources.apply_dpi

local brightness = wibox.widget.textbox()
brightness.font = beautiful.font

watch([[bash -c "brightnessctl | grep -oP '[^()]+%'"]], 2, function(_, stdout)
    brightness.text = stdout
    collectgarbage('collect')
end)


brightness_icon = wibox.widget {
	markup = '<span font="' .. theme.icon_font .. '"foreground="'.. colors.brightblue ..'"> </span>',
	widget = wibox.widget.textbox,
}

return wibox.widget {
	brightness_icon,
    wibox.widget{
        brightness, 
        fg = colors.brightyellow,
        widget = wibox.container.background
    },
    spacing = dpi(2),
    layout = wibox.layout.fixed.horizontal
}
