-- ## Logo ##
-- ~~~~~~~~~~~~~~~~

-- requirements
-- ~~~~~~~~~~~~
local wibox = require('wibox')
local watch = require('awful.widget.watch')
local beautiful = require('beautiful')
local dpi = require('beautiful').xresources.apply_dpi


logo_icon = wibox.widget {
	markup = '<span font="' .. theme.ui_font ..'"> </span>',
	widget = wibox.widget.textbox,
}

return wibox.widget {
    wibox.widget{
        logo_icon, 
        bg = colors.main_scheme,
		left = dpi(8),
		right = dpi(2),
		widget = wibox.container.margin,
    },
    layout = wibox.layout.fixed.horizontal
}


--awful.mouse.append_global_mousebindings({
--    awful.button({ }, 3, function () mymainmenu:toggle() end)
--})
