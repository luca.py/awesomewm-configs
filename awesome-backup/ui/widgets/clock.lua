-- ## Clock ##
-- ~~~~~~~~~~~~~

-- requirements
-- ~~~~~~~~~~~~
local wibox = require('wibox')
local beautiful = require('beautiful')
local dpi = require('beautiful').xresources.apply_dpi

--local clock = wibox.widget.textclock('<span font="' .. theme.font .. '">%a %d %b | %H:%M</span>')
local clock = wibox.widget.textclock('<span font="' .. theme.font .. '">%H:%M</span>')

--return clock
clock_icon = wibox.widget {
	markup = '<span font="' .. theme.icon_font .. '"foreground="'.. colors.main_scheme ..'"> </span>',
	widget = wibox.widget.textbox,
}

return wibox.widget {
	clock_icon,
    wibox.widget{
        clock, 
        fg = colors.brightwhite,
        widget = wibox.container.background
    },
    spacing = dpi(2),
    layout = wibox.layout.fixed.horizontal
}
