-- Standard awesome library
pcall(require, "luarocks.loader")
require("awful.autofocus")
local awful         = require("awful")
-- Theme handling library
local beautiful 	= require("beautiful")


-- # Themes :
local theme = require("themes")
beautiful.init(theme)

-- # Keybindings :
require("configurations.keybindings")

-- # Layouts :
require("configurations.layouts")

-- # Tags :
--require("configurations.tags") -- if you use ghost use this :)

-- # Rules :
require("configurations.rules")

-- # Signals :
require("signals")

-- # Titlebars :
require("ui.titlebar")

-- # Notifications
require("ui.notifications")

-- # Menu :
require("ui.menu")

-- # Bar :
require("ui.bar")
--require("ui.bar-powerline")

-- # Sidebar :
require("ui.sidebar")

-- Autorun at startup
awful.spawn.with_shell("bash ~/.config/awesome/configurations/autorun")

--- Enable for lower memory consumption
collectgarbage("setpause", 110)
collectgarbage("setstepmul", 1000)
