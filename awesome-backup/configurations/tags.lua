-- ## Tag ##
-- ~~~~~~~~~

-- requirements
-- ~~~~~~~~~~~~
local awful = require("awful")

-- Tags :
awful.util.tagnames =  { "", "", "", "", "", "", "", "", ""}
--awful.util.tagnames =  { "1", "2", "3", "4", "5", "6", "7", "8", "9" }

-- Each screen has its own tag table.
screen.connect_signal("request::desktop_decoration", function(s)
	awful.tag(awful.util.tagnames, s, awful.layout.layouts[1])
end)
